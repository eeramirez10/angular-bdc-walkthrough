import { Component, OnInit } from '@angular/core';
import { BdcWalkService } from 'bdc-walkthrough';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {

  cards: any[] = [
    {
      img: 'https://images.unsplash.com/photo-1647196646157-840f334ec787?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHx0b3BpYy1mZWVkfDR8cm5TS0RId3dZVWt8fGVufDB8fHx8&auto=format&fit=crop&w=500&q=60',
      title: 'Card title',
      text: `Some quick example text to build on the card title and make up the bulk of the
      card's
      content.`,
      btnTxt: 'Go somewhere'
    },
    {
      img: 'https://images.unsplash.com/photo-1647083237594-958754cb1cd1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHx0b3BpYy1mZWVkfDN8cm5TS0RId3dZVWt8fGVufDB8fHx8&auto=format&fit=crop&w=500&q=60',
      title: 'Card title',
      text: `Some quick example text to build on the card title and make up the bulk of the
      card's
      content.`,
      btnTxt: 'Go somewhere'
    },
    {
      img: 'https://images.unsplash.com/photo-1647258607189-f0bae637f02c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHx0b3BpYy1mZWVkfDh8cm5TS0RId3dZVWt8fGVufDB8fHx8&auto=format&fit=crop&w=500&q=60',
      title: 'Card title',
      text: `Some quick example text to build on the card title and make up the bulk of the
      card's
      content.`,
      btnTxt: 'Go somewhere'
    },
    {
      img: 'https://images.unsplash.com/photo-1647196646157-840f334ec787?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHx0b3BpYy1mZWVkfDR8cm5TS0RId3dZVWt8fGVufDB8fHx8&auto=format&fit=crop&w=500&q=60',
      title: 'Card title',
      text: `Some quick example text to build on the card title and make up the bulk of the
      card's
      content.`,
      btnTxt: 'Go somewhere'
    }

  ]

  constructor(private bdcWalkService: BdcWalkService) { }

  ngOnInit(): void {
  }

  startTour(){

  


      this.bdcWalkService.reset()

      
   

    this.bdcWalkService.setTaskCompleted('startCardsTour',true)



   

  }

  close(){

    this.bdcWalkService.setTaskCompleted('startCardsTour',false)

  }

}
