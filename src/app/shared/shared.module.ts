import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card/card.component';
import { BdcWalkModule } from 'bdc-walkthrough';
import { NavbarComponent } from './navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { CarouselComponent } from './carousel/carousel.component';



@NgModule({
  declarations: [
    CardComponent,
    NavbarComponent,
    CarouselComponent
  ],
  exports:[
    CardComponent,
    NavbarComponent,
    CarouselComponent
  ],
  imports: [
    CommonModule,
    BdcWalkModule,
    RouterModule
  ]
})
export class SharedModule { }
