import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {

  carousel:any = {
    img: 'https://images.unsplash.com/photo-1640622843377-6b5af9417e70?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
    title: 'Card title',
    text: `Some quick example text to build on the card title and make up the bulk of the
    card's
    content.`,
    btnTxt: 'Go somewhere'
  };

  constructor() { }

  ngOnInit(): void {
  }

}
