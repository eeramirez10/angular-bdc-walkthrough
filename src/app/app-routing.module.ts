import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardsComponent } from './tour/cards/cards.component';
import { MainComponent } from './tour/main/main.component';
import { AboutComponent } from './tour/about/about.component';


const routes: Routes = [
  { path:'main', component:MainComponent},
  { path:'cards', component:CardsComponent},
  { path:'about', component:AboutComponent},
  { path:'', redirectTo:'/main', pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
